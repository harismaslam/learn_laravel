<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
  return view('welcome');
});

Route::get('about', 'PagesController@about');

Route::get('contact', 'PagesController@contact');

Route::get('hello/{name}', function ($name) {
  echo 'Hello there ' . $name;
});



//Route::get('customer/{id}', function ($id) {
//  $customer = App\Customer::find($id);
//  echo $customer->name.'<br/>';
//  echo 'orders';
//  $orders = $customer->orders;
//  echo '<ul>';
//  foreach ($orders as $order){
//    echo '<li>'.$order->name.'</li>';
//  }
//});
//creating the controller
Route::get('customer/{id}', 'CustomerController@customer');

Route::get('customer_name', function () {
  $customer = App\Customer::where('name', '=', 'Haris')->first();
  echo $customer->id;
});

//Route::get('orders', function () {
//  $orders = App\Order::all();
//  foreach ($orders as $order) {
//    $customer = App\Customer::find($order->customer_id);
//    echo $order->name . ' ordered by '.$customer->name.'<br/>';
//  }
//});

Route::get('orders', function () {
  $orders = App\Order::all();
  foreach ($orders as $order) {
    echo $order->name . ' ordered by ' . $order->customer->name . '<br/>';
  }
});

Route::get('mypage', function () {
  $data = array(
    'var1' => 'Chappathi',
    'var2' => 'Porotta',
    'var3' => 'Beef fry',
    'orders' => App\Order::all()
  );
  return view('mypage', $data);
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'web'], function () {
  Route::post('test', function () {
    echo 'POST';
  });

  Route::get('test', function () {
    echo '<form method="POST" action="test">';
//  echo csrf_field();
    echo '<input type="submit">';
    echo '<input type="hidden" name="_token" value="' . csrf_token() . '">';
//  echo '<input type="hidden" value="DELETE" name="_method"/>';
    echo '</form>';
  });

  Route::delete('test', function () {
    echo 'DELETE';
  });
  Route::put('test', function () {
    echo 'PUT';
  });

//  Route::get('access', function(){
//    echo 'You have access';
//  })->middleware('auth');
});

Route::get('form', function() {
  return view('form');
});

use Illuminate\Http\Request;

Route::post('post_to_me', function(Request $request) {
  echo $request->input('name');
});

Route::get('access', function() {
  echo 'You have access';
})->middleware('isAdmin');


Route::get('cards', 'CardsController@index');
Route::get('cards/{card}', 'CardsController@show');