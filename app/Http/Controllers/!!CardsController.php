<?php
/*
 * It is using query builder
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class CardsController extends Controller {

  //
  public function index() {
//    $cards = \DB::table('cards')->get();
    $cards = DB::table('cards')->get();
    return view('cards.index', compact('cards'));
  }

  public function show($card) {
    $card = DB::table('cards')->where('id',$card)->first();
    return view('cards.show', compact('card'));
  }
}
