<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Customer as Customer;

class CustomerController extends Controller {

  //
  public function customer($id) {
//    $customer = \App\Customer::find($id);
    //if we use the next line then declare the namespace
    $customer = Customer::find($id);
//    echo $customer->name . '<br/>';
//    echo 'orders';
//    $orders = $customer->orders;
//    echo '<ul>';
//    foreach ($orders as $order) {
//      echo '<li>' . $order->name . '</li>';
//    }
    return view('customer', array('customer'=>$customer));
  }

}
